package com.criscar.springbootmvcrestapi.services;
import java.util.List;

import com.criscar.springbootmvcrestapi.entities.Car;
import com.criscar.springbootmvcrestapi.repositories.CarRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarSercviceImpl implements CarService {

	private CarRepository carRepository;

	@Autowired
	public CarSercviceImpl(final CarRepository carRepository) {
		this.carRepository = carRepository;
	}

	@Override	
	public Car getCarById(Integer carId) {
		return this.carRepository.getCarById(carId);
	}

	@Override
	public void addCar(Car car) {
		this.carRepository.addCar(car);
	}

	@Override
	public List<Car> getAllCars() {
		return this.carRepository.getAllCars();
	}
}