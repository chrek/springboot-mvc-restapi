package com.criscar.springbootmvcrestapi.services;

import java.util.List;

import com.criscar.springbootmvcrestapi.entities.Car;

public interface CarService {
		Car getCarById(Integer carId);
		void addCar(Car car);
		List<Car> getAllCars();
}