package com.criscar.springbootmvcrestapi.repositories;
import java.util.List;

import com.criscar.springbootmvcrestapi.entities.Car;

public interface CarRepository {
    Car getCarById(long carId);
    void addCar(Car car);
    List<Car> getAllCars();
}