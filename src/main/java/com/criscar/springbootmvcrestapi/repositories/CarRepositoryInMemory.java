package com.criscar.springbootmvcrestapi.repositories;

import java.util.ArrayList;
import java.util.List;

import com.criscar.springbootmvcrestapi.entities.Car;

import org.springframework.stereotype.Repository;

//import org.springframework.http.StreamingHttpOutputMessage.Body;

@Repository
public class CarRepositoryInMemory implements CarRepository {
    private ArrayList<Car> cars;


	public CarRepositoryInMemory() {
        this.cars = new ArrayList<>();
	}

    public Car getCarById(long carId) {
        for (Car c : this.cars) {
            if (c.getId() == carId) {
                return c;
            }
        }
        return null;
    }
    public void addCar(Car car) {
        this.cars.add(car);
    }
    
    public List<Car> getAllCars() {
        return this.cars;
    }
}