package com.criscar.springbootmvcrestapi.controllers;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import com.criscar.springbootmvcrestapi.entities.Car;
import com.criscar.springbootmvcrestapi.services.CarService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Api(tags = "Car management")
public class CarController {

    private CarService carService;
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    public CarController(final CarService carService) {
        this.carService = carService;
    }

    @RequestMapping(value = "/cars/{carId}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get car by ID")
    Car getCarById(@PathVariable final Integer carId) {
        return this.carService.getCarById(carId);
		}
		
    @RequestMapping(value = "/cars", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get all cars")
    List<Car> getAllCars() {
        return this.carService.getAllCars();
    }
    
    @RequestMapping(value = "/cars", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)     
    @ApiOperation(value = "Add a car") 
    void addCar(@RequestBody Car car){
     this.carService.addCar(car); 
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/car")
    public Car carRecord(@RequestParam(value = "name", defaultValue = "308 HDI") String name) {
        return new Car(counter.incrementAndGet(), name, "Peugeot");
    }

}
