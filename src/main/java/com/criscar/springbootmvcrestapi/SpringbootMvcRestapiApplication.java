package com.criscar.springbootmvcrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMvcRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMvcRestapiApplication.class, args);
	}

}

