# Angular7Bootstrap4

This project was generated with [Spring Initializr](https://start.spring.io/) version 2.1.2.

The Spring Boot project is used to create a REST API based on the MVC architecture.
 
It also incorporates the Swagger documentation.

## Development server
Run `mvnw spring-boot:run` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.

## Swagger documentation
With swagger features implemented, a page of swagger - ui.html will be available with a list of controllers and their associated methods: http://localhost:8080/swagger-ui.html#/

## Reference
[Atomrace](https://atomrace.com/developper-une-api-rest-avec-spring-boot/).
